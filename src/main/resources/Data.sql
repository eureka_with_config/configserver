CREATE TABLE properties 
  ( 
     application VARCHAR(200), 
     profile     VARCHAR(200), 
     label       VARCHAR(200), 
     key         VARCHAR(200), 
     value       VARCHAR(200) 
  );
 
INSERT INTO properties (application, profile, label, key, value)
VALUES ('product-service','production','latest','datasource-driver-class-name','MyDriverClass');

INSERT INTO properties (application, profile, label, key, value)
VALUES ('product-service','production','latest','spring.datasource.url','jdbc:h2:mem:testdb');

INSERT INTO properties (application, profile, label, key, value)
VALUES ('product-service','production','latest','spring.datasource.driverClassName','org.h2.Driver');

INSERT INTO properties (application, profile, label, key, value)
VALUES ('product-service','production','latest','spring.datasource.username','sql9363492');

INSERT INTO properties (application, profile, label, key, value)
VALUES ('product-service','production','latest','spring.datasource.password','password');